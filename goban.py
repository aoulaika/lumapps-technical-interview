import enum
from typing import List, Set, Tuple


class Status(enum.Enum):
    """
    Enum representing the Status of a position on a goban
    """

    WHITE = 1
    BLACK = 2
    EMPTY = 3
    OUT = 4


class Goban:
    def __init__(self, goban: List[str] = []):
        self.goban = goban

    def get_status(self, x: int, y: int):
        """Get the status of a given position

        Arguments:
            x {int} -- the x-coordinate
            y {int} -- the y-coordinate

        Returns:
            Status -- the status of the given coordinates.
        """
        if (
            not self.goban
            or x < 0
            or y < 0
            or y >= len(self)
            or x >= len(self.goban[0])
        ):
            return Status.OUT
        elif self.goban[y][x] == ".":
            return Status.EMPTY
        elif self.goban[y][x] == "o":
            return Status.WHITE
        elif self.goban[y][x] == "#":
            return Status.BLACK

    def is_taken(self, x: int, y: int, checked: Set[Tuple[int, int]] = None):
        """check if the stone with the position x is taken.

        Arguments:
            x {int} -- the x-coordinate
            y {int} -- the y-coordinate
            checked {set} -- a set of tuples (coordinates) already checked

        Returns:
            bool -- True if the stone is taken, False otherwise
        """
        # it would be better setting checked by default to an empty set but Python don't like that :D
        if checked is None:
            checked = set()

        checked.add((x, y))

        # Status.OUT will never occurse on any recursion,
        # but needed to handle OUT coordinates if given (first call to the function)
        if self.get_status(x, y) in (Status.OUT, Status.EMPTY):
            return False

        # we only need to check if there's at least one freedom
        if next(self._freedoms(x, y), None) is not None:
            return False

        # check recursively the stone shape if they are not taken
        # minus what we already checked
        to_check = set(self._shape(x, y)) - checked
        for c in to_check:
            checked.add(c)
            # if any shape's element is not taken, we stop the recursion
            # in this case our origin stone is not taken
            if not self.is_taken(*c, checked):
                return False

        # unfortunately that stone is taken :(
        return True

    def _freedoms(self, x: int, y: int):
        """get all freedoms (empty adjacents) for a given stone.

        Arguments:
            x {int} -- the x-coordinate
            y {int} -- the y-coordinate

        Returns:
            filter object -- a generator of coordinates (tuples) of freedoms.
        """
        return self.__adjacents(x, y, Status.EMPTY)

    def _shape(self, x: int, y: int):
        """get a group of adjacent stones with the same status as the given one (x,y).

        Arguments:
            x {int} -- the x-coordinate
            y {int} -- the y-coordinate

        Returns:
            filter object -- a generator of coordinates (tuples) of stones with same status.
        """
        return self.__adjacents(x, y, self.get_status(x, y))

    def __adjacents(self, x: int, y: int, status: Status):
        """get adjacents stones for a given one, filtere by a given status.

        Arguments:
            x {int} -- the x-coordinate
            y {int} -- the y-coordinate
            status {Status} -- the status of the adjacents stones to retrieve.

        Returns:
            filter object -- a generator of coordinates (tuples) of all adjacents stones with same status.
        """
        return filter(
            lambda c: self.get_status(*c) == status,
            ((x, y - 1), (x, y + 1), (x - 1, y), (x + 1, y)),
        )

    # helper to provide Goban' lines count
    def __len__(self):
        """Goban' len, the number of lines.

        Returns:
            int -- number of lines in the goban list
        """
        return len(self.goban)

    # used during debugging to provide pretty print of Goban instances
    def __str__(self):
        tab, nl = "\t", "\n"
        return f"Goban([{nl}{nl.join(f'{tab}{tab.join(i)}' for i in self.goban)}{nl}])"

    # used during debugging to provide pretty print of Goban instances
    def __repr__(self):
        return f"Goban({self.goban})"
