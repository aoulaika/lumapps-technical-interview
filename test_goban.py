from goban import Goban


def test_white_is_taken_when_surrounded_by_black():
    goban = Goban([".#.", "#o#", ".#."])

    assert goban.is_taken(1, 1) is True


def test_white_is_not_taken_when_it_has_a_liberty():
    goban = Goban(["...", "#o#", ".#."])

    assert goban.is_taken(1, 1) is False


def test_black_shape_is_taken_when_surrounded():
    goban = Goban(["oo.", "##o", "o#o", ".o."])

    assert goban.is_taken(0, 1) is True
    assert goban.is_taken(1, 1) is True
    assert goban.is_taken(1, 2) is True


def test_black_shape_is_not_taken_when_it_has_a_liberty():
    goban = Goban(["oo.", "##.", "o#o", ".o."])

    assert goban.is_taken(0, 1) is False
    assert goban.is_taken(1, 1) is False
    assert goban.is_taken(1, 2) is False


def test_square_shape_is_taken():
    goban = Goban(["oo.", "##o", "##o", "oo."])

    assert goban.is_taken(0, 1) is True
    assert goban.is_taken(0, 2) is True
    assert goban.is_taken(1, 1) is True
    assert goban.is_taken(1, 2) is True


def test_big_goban_is_taken():
    goban = Goban(
        [
            ".................................................o#",
            "................................................o#o",
            "...............................................o#oo",
            "..............................................o#ooo",
            ".............................................o#oooo",
            "............................................o#ooooo",
            "...........................................o#oooooo",
            "..........................................o#ooooooo",
            ".........................................o#oooooooo",
            "........................................o#ooooooooo",
            ".......................................o#oooooooooo",
            "......................................o#ooooooooooo",
            ".....................................o#oooooooooooo",
            "....................................o#ooooooooooooo",
            "...................................o#oooooooooooooo",
            "..................................o#ooooooooooooooo",
            ".................................o#oooooooooooooooo",
            "................................o#ooooooooooooooooo",
            "...............................o#oooooooooooooooooo",
            "..............................o#ooooooooooooooooooo",
            ".............................o#oooooooooooooooooooo",
            "............................o#ooooooooooooooooooooo",
            "...........................o#oooooooooooooooooooooo",
            "..........................o#ooooooooooooooooooooooo",
            ".........................o#oooooooooooooooooooooooo",
            "........................o#ooooooooooooooooooooooooo",
            ".......................o#oooooooooooooooooooooooooo",
            "......................o#ooooooooooooooooooooooooooo",
            ".....................o#oooooooooooooooooooooooooooo",
            "....................o#ooooooooooooooooooooooooooooo",
            "...................o#oooooooooooooooooooooooooooooo",
            "..................o#ooooooooooooooooooooooooooooooo",
            ".................o#oooooooooooooooooooooooooooooooo",
            "................o#ooooooooooooooooooooooooooooooooo",
            "...............o#oooooooooooooooooooooooooooooooooo",
            "..............o#ooooooooooooooooooooooooooooooooooo",
            ".............o#oooooooooooooooooooooooooooooooooooo",
            "............o#ooooooooooooooooooooooooooooooooooooo",
            "...........o#oooooooooooooooooooooooooooooooooooooo",
            "..........o#ooooooooooooooooooooooooooooooooooooooo",
            ".........o#oooooooooooooooooooooooooooooooooooooooo",
            "........o#ooooooooooooooooooooooooooooooooooooooooo",
            ".......o#oooooooooooooooooooooooooooooooooooooooooo",
            "......o#ooooooooooooooooooooooooooooooooooooooooooo",
            ".....o#oooooooooooooooooooooooooooooooooooooooooooo",
            "....o#ooooooooooooooooooooooooooooooooooooooooooooo",
            "...o#oooooooooooooooooooooooooooooooooooooooooooooo",
            "..o#ooooooooooooooooooooooooooooooooooooooooooooooo",
            ".o#oooooooooooooooooooooooooooooooooooooooooooooooo",
            "o#ooooooooooooooooooooooooooooooooooooooooooooooooo",
        ]
    )

    assert goban.is_taken(50, 0) is True


def test_super_big_goban_is_taken():
    goban = Goban(
        [
            "...................................................................................................o#",
            "..................................................................................................o#o",
            ".................................................................................................o#oo",
            "................................................................................................o#ooo",
            "...............................................................................................o#oooo",
            "..............................................................................................o#ooooo",
            ".............................................................................................o#oooooo",
            "............................................................................................o#ooooooo",
            "...........................................................................................o#oooooooo",
            "..........................................................................................o#ooooooooo",
            ".........................................................................................o#oooooooooo",
            "........................................................................................o#ooooooooooo",
            ".......................................................................................o#oooooooooooo",
            "......................................................................................o#ooooooooooooo",
            ".....................................................................................o#oooooooooooooo",
            "....................................................................................o#ooooooooooooooo",
            "...................................................................................o#oooooooooooooooo",
            "..................................................................................o#ooooooooooooooooo",
            ".................................................................................o#oooooooooooooooooo",
            "................................................................................o#ooooooooooooooooooo",
            "...............................................................................o#oooooooooooooooooooo",
            "..............................................................................o#ooooooooooooooooooooo",
            ".............................................................................o#oooooooooooooooooooooo",
            "............................................................................o#ooooooooooooooooooooooo",
            "...........................................................................o#oooooooooooooooooooooooo",
            "..........................................................................o#ooooooooooooooooooooooooo",
            ".........................................................................o#oooooooooooooooooooooooooo",
            "........................................................................o#ooooooooooooooooooooooooooo",
            ".......................................................................o#oooooooooooooooooooooooooooo",
            "......................................................................o#ooooooooooooooooooooooooooooo",
            ".....................................................................o#oooooooooooooooooooooooooooooo",
            "....................................................................o#ooooooooooooooooooooooooooooooo",
            "...................................................................o#oooooooooooooooooooooooooooooooo",
            "..................................................................o#ooooooooooooooooooooooooooooooooo",
            ".................................................................o#oooooooooooooooooooooooooooooooooo",
            "................................................................o#ooooooooooooooooooooooooooooooooooo",
            "...............................................................o#oooooooooooooooooooooooooooooooooooo",
            "..............................................................o#ooooooooooooooooooooooooooooooooooooo",
            ".............................................................o#oooooooooooooooooooooooooooooooooooooo",
            "............................................................o#ooooooooooooooooooooooooooooooooooooooo",
            "...........................................................o#oooooooooooooooooooooooooooooooooooooooo",
            "..........................................................o#ooooooooooooooooooooooooooooooooooooooooo",
            ".........................................................o#oooooooooooooooooooooooooooooooooooooooooo",
            "........................................................o#ooooooooooooooooooooooooooooooooooooooooooo",
            ".......................................................o#oooooooooooooooooooooooooooooooooooooooooooo",
            "......................................................o#ooooooooooooooooooooooooooooooooooooooooooooo",
            ".....................................................o#oooooooooooooooooooooooooooooooooooooooooooooo",
            "....................................................o#ooooooooooooooooooooooooooooooooooooooooooooooo",
            "...................................................o#oooooooooooooooooooooooooooooooooooooooooooooooo",
            "..................................................o#ooooooooooooooooooooooooooooooooooooooooooooooooo",
            ".................................................o#oooooooooooooooooooooooooooooooooooooooooooooooooo",
            "................................................o#ooooooooooooooooooooooooooooooooooooooooooooooooooo",
            "...............................................o#oooooooooooooooooooooooooooooooooooooooooooooooooooo",
            "..............................................o#ooooooooooooooooooooooooooooooooooooooooooooooooooooo",
            ".............................................o#oooooooooooooooooooooooooooooooooooooooooooooooooooooo",
            "............................................o#ooooooooooooooooooooooooooooooooooooooooooooooooooooooo",
            "...........................................o#oooooooooooooooooooooooooooooooooooooooooooooooooooooooo",
            "..........................................o#ooooooooooooooooooooooooooooooooooooooooooooooooooooooooo",
            ".........................................o#oooooooooooooooooooooooooooooooooooooooooooooooooooooooooo",
            "........................................o#ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo",
            ".......................................o#oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo",
            "......................................o#ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo",
            ".....................................o#oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo",
            "....................................o#ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo",
            "...................................o#oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo",
            "..................................o#ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo",
            ".................................o#oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo",
            "................................o#ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo",
            "...............................o#oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo",
            "..............................o#ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo",
            ".............................o#oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo",
            "............................o#ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo",
            "...........................o#oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo",
            "..........................o#ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo",
            ".........................o#oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo",
            "........................o#ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo",
            ".......................o#oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo",
            "......................o#ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo",
            ".....................o#oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo",
            "....................o#ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo",
            "...................o#oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo",
            "..................o#ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo",
            ".................o#oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo",
            "................o#ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo",
            "...............o#oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo",
            "..............o#ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo",
            ".............o#oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo",
            "............o#ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo",
            "...........o#oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo",
            "..........o#ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo",
            ".........o#oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo",
            "........o#ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo",
            ".......o#oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo",
            "......o#ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo",
            ".....o#oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo",
            "....o#ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo",
            "...o#oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo",
            "..o#ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo",
            ".o#oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo",
            "o#ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo",
        ]
    )

    assert goban.is_taken(100, 0) is True
